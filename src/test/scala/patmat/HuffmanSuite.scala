package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    val t3 = Leaf('k', 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
      assert(weight(t2) === 9)
      assert(weight(t3) === 9)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t1) === List('a', 'b'))
      assert(chars(t2) === List('a','b','d'))
      assert(chars(t3) === List('k'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times over Nil gives Nil") {
    assert(times(Nil) === Nil)
  }

  test("times over single value list gives a singleton result") {
    assert(times(List('a')) === List(('a', 1)))
  }

  test("times over unique values gives singleton results for them") {
    assert(times(List('a', 'b', 'c', 'd', 'e')) === List(('a', 1), ('b', 1), ('c', 1), ('d', 1), ('e', 1)))
  }

  test("times over repeated values gives one result with higher ref count") {
    assert(times(List('a', 'a', 'a')) === List(('a', 3)))
  }

  test("times over mixture of values gives histogram ordered by order of distinct values encountered") {
    assert(times(List('a', 'd', 'c', 'd', 'b', 'g', 'e', 'e', 'd', 'f', 'c'))
           === List(('a', 1), ('d', 3), ('c', 2), ('b', 1), ('g', 1), ('e', 2), ('f', 1)))
  }

  test("orderedInsert with Nil list gives singleton list with value to insert") {
    new TestTrees {
      assert(orderedInsert(Leaf('a', 1), Nil) === List(Leaf('a', 1)))
      assert(orderedInsert(t1, Nil) === List(t1))
    }
  }

  test("orderedInsert with higher-valued singleton value gives list in ascending order") {
    new TestTrees {
      assert(orderedInsert(Leaf('@', -1), List(t1)) === List(Leaf('@', -1), t1))
    }
  }

  test("orderedInsert with lower-valued singleton value gives list in ascending order") {
    new TestTrees {
      assert(orderedInsert(Leaf('z', 99), List(t1)) === List(t1, Leaf('z', 99)))
    }
  }

  test("orderedInsert with value that falls in the middle gives list with it in the middle") {
    new TestTrees {
      assert(orderedInsert(Leaf('m', 6), List(Leaf('a', 1), Leaf('b', 2),
                                              Leaf('x', 8), Leaf('y', 9))) ===
             List(Leaf('a', 1), Leaf('b', 2), Leaf('m', 6), Leaf('x', 8),
                  Leaf('y', 9)))
    }
  }

  test("makeOrderedLeafList returns Nil for Nil") {
    assert(makeOrderedLeafList(Nil) === Nil)
  }

  test("makeOrderedLeafList returns a leaf for singleton input") {
    assert(makeOrderedLeafList(List(('a', 1))) === List(Leaf('a', 1)))
  }

  test("makeOrderedLeafList returns reversed leaves for reversed list") {
    assert(makeOrderedLeafList(List(('z', 9), ('w', 7), ('u', 5), ('s', 3),
                                    ('q', 1))) ===
           List(Leaf('q', 1), Leaf('s', 3), Leaf('u', 5), Leaf('w', 7),
                Leaf('z', 9)))
  }

  test("makeOrderedLeafList returns same order of leaves for sorted input") {
    assert(makeOrderedLeafList(List(('q', 1), ('s', 3), ('u', 5), ('w', 7),
                                    ('z', 9))) ===
           List(Leaf('q', 1), Leaf('s', 3), Leaf('u', 5), Leaf('w', 7),
                Leaf('z', 9)))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("singleton on Nil is false") {
    assert(!singleton(Nil))
  }

  test("singleton on single code tree node is true") {
    assert(singleton(List(Leaf('a', 1))))
  }

  test("singleton on more than one code tree node is false") {
    assert(!singleton(List(Leaf('a', 1), Leaf('b', 1))))
  }

  test("combine on Nil is Nil") {
    assert(combine(Nil) === Nil)
  }

  test("combine on singleton list is the original list") {
    new TestTrees {
      assert(combine(List(t1)) === List(t1))
    }
  }

  test("combine on multiple entry list is a singleton list") {
    new TestTrees {
      assert(singleton(combine(List(t2, t3))))
      assert(weight(combine(List(t2, t3)).head) === weight(t2) + weight(t3))
    }
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("until with predicate that is always true returns trees as-is") {
    new TestTrees {
      val result = until(_ => true, _ => Nil)(List(t2, t3))
      assert(result === List(t2, t3))
    }
  }

  test("until with predicate that is false once then true invokes transform once") {
    new TestTrees {
      var predCount = 0
      var transformCount = 0
      val result = until({ (xs: List[CodeTree]) =>
        predCount += 1
        (weight(xs.head) & 1) == 0
      }, {
        transformCount += 1
        _ match {
          case List(Fork(l, r, chs, w)) => List(Fork(l, r, chs, w * 2))
        }
      })(List(t1))
      assert(predCount === 2)
      assert(transformCount === 1)
      assert((result match {
        case List(Fork(l, r, chs, w)) => w
      }) === 2 * (t1 match {
        case Fork(l, r, chs, w) => w
      }))
    }
  }

  test("createCodeTree throws IllegalStateException when invoked on Nil tree") {
    intercept[IllegalStateException] {
      createCodeTree(Nil)
    }
  }

  test("createCodeTree produces an encoded tree given input text") {
    val text = string2Chars("Addis Ababa")
    val codeTree = createCodeTree(text)
    assert(weight(codeTree) === 11)
  }

  test("decode can decode bits that form paths in the Huffman tree") {
    val codeTree =
      Fork(
        Fork(
          Leaf('a', 9),
          Leaf('c', 11),
          string2Chars("ac"), 20),
        Fork(
          Leaf('b', 7),
          Leaf('d', 14),
          string2Chars("bd"), 21),
        string2Chars("acbd"), 41)
    val bitString = List(0, 0, 0, 1, 1, 0, 1, 1)
    assert(decode(codeTree, bitString) === List('a', 'c', 'b', 'd'))
  }

  test("decode throws IllegalArgumentException for paths that do not lead to leaves") {
    intercept[IllegalArgumentException] {
      val codeTree =
        Fork(
          Fork(
            Leaf('a', 9),
            Leaf('c', 11),
            string2Chars("ac"), 20),
          Fork(
            Leaf('b', 7),
            Leaf('d', 14),
            string2Chars("bd"), 21),
          string2Chars("acbd"), 41)
      val bitString = List(0, 1, 1)
      decode(codeTree, bitString)
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("French string decoding") {
    assert(decodedSecret === string2Chars("huffmanestcool"))
  }

  test("Encoding for Nil is Nil") {
    new TestTrees {
      assert(encode(t3)(Nil) === Nil)
    }
  }

  test("Encoding for a tree provides the character at the end of the path") {
    val codeTree =
      Fork(
        Fork(
          Leaf('a', 9),
          Leaf('c', 11),
          string2Chars("ac"), 20),
        Fork(
          Leaf('b', 7),
          Leaf('d', 14),
          string2Chars("bd"), 21),
        string2Chars("acbd"), 41)
    val result = encode(codeTree)(List('b'))
    val expectedResult = List(1, 0)
    assert(result === expectedResult)
  }

  test("Encoding for a string provides a bitstream") {
    val result = encode(frenchCode)(string2Chars("huffmanestcool"))
    assert(result === secret)
  }

  test("Bad encoding results in IllegalStateException") {
    intercept[IllegalStateException] {
      val codeTree =
        Fork(
          Fork(
            Leaf('a', 9),
            Leaf('c', 11),
            string2Chars("ac"), 20),
          Fork(
            Leaf('b', 7),
            Leaf('d', 14),
            string2Chars("bd"), 21),
          string2Chars("abd"), 41) // missing 'c'
      encode(codeTree)(List('c'))
    }
  }

  test("codeBits: French text") {
    assert(quickEncode(frenchCode)(string2Chars("huffmanestcool")) === secret)
  }
}
